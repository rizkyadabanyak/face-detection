import React, {useEffect, useReducer, useState} from 'react'
import { useHistory } from "react-router-dom";

const Clarifai = require('clarifai');

const app = new Clarifai.App({
    apiKey: '609300a4c2184ca690131b39989f24ec'
});

const formReducer = (state, event) => {
    return {
        ...state,
        [event.name]: event.value
    }
}

const AddList = ({ token, setAntri }) =>{
    const [formData, setFormData] = useReducer(formReducer, {});
    const [imageUrl, setImageUrl] = useState("");
    const [imageLocal, setImageLocal] = useState("");
    const [urlBox, setUrlBox] = useState([]);
    const [localBox, setLocalBox] = useState([]);
    const [showUrlBoxes, setShowUrlBoxes] = useState([]);
    const [showLocalBoxes, setShowLocalBoxes] = useState([]);
    const [urlState, setUrlState] = useState(false);
    const [urlError, setUrlError] = useState(false);
    const [localState, setLocalState] = useState(false);
    const [localError, setLocalError] = useState(false);
    let history = useHistory();

    // useEffect(() => {
       
    //     getAntriStatus();
    // });

    const calculateUrl = (data) => {
        const outputs = data.outputs[0].data;
        const urlImage = document.getElementById("urlFace");
        const urlImageWidth = urlImage.width;
        const urlImageHeight = urlImage.height;
        const arr = urlBox;

        if(outputs.regions.length == 1) { 
            setUrlState(true); 
            setUrlError(false);
        } else { 
            setUrlState(false);
            setUrlError(true); 
        }

        for(let i = 0; i < outputs.regions.length; i++) {
            const clarifaiRegions = outputs.regions[i].region_info.bounding_box;

            arr.push({
                leftCol: clarifaiRegions.left_col * urlImageWidth,
                topRow: clarifaiRegions.top_row * urlImageHeight,
                rightCol: urlImageWidth - (clarifaiRegions.right_col * urlImageWidth),
                bottomRow: urlImageHeight - (clarifaiRegions.bottom_row * urlImageHeight)
            })
        }
        
        setUrlBox(arr);
        let urlBoxes = [];
        for(let i = 0; i < urlBox.length; i++) {
            urlBoxes.push(
                <div key={i} className="bounding-box" style={{top: urlBox[i].topRow, right: urlBox[i].rightCol, bottom: urlBox[i].bottomRow, left: urlBox[i].leftCol }}></div>
            )
        }

        setShowUrlBoxes(urlBoxes);
    }

    const calculateLocal = (data) => {
        const outputs = data.outputs[0].data;
        const localImage = document.getElementById("localFace");
        const localImageWidth = localImage.width;
        const localImageHeight = localImage.height;
        const arr = localBox;

        if(outputs.regions.length == 1) { 
            setLocalState(true); 
            setLocalError(false);
        } else { 
            setLocalState(false);
            setLocalError(true); 
        }

        for(let i = 0; i < outputs.regions.length; i++) {
            const clarifaiRegions = outputs.regions[i].region_info.bounding_box;

            arr.push({
                leftCol: clarifaiRegions.left_col * localImageWidth,
                topRow: clarifaiRegions.top_row * localImageHeight,
                rightCol: localImageWidth - (clarifaiRegions.right_col * localImageWidth),
                bottomRow: localImageHeight - (clarifaiRegions.bottom_row * localImageHeight)
            })
        }

        setLocalBox(arr);
        let localBoxes = [];
        for(let i = 0; i < localBox.length; i++) {
            localBoxes.push(
                <div key={i} className="bounding-box" style={{top: localBox[i].topRow, right: localBox[i].rightCol, bottom: localBox[i].bottomRow, left: localBox[i].leftCol }}></div>
            )
        }

        setShowLocalBoxes(localBoxes);
    }

    const validateUrl = event => {
        event.preventDefault();
        app.models.predict(
            Clarifai.FACE_DETECT_MODEL,
            formData.url)
            .then(
                function(response) {
                    calculateUrl(response);
                },
                function(err) {
                    console.log(err);
                }
            );
    }   
    
    const validateLocal = event => {
        event.preventDefault();
        const image64 = imageLocal.replace(/^data:image\/(.*);base64,/, '');
        app.models.predict(
            Clarifai.FACE_DETECT_MODEL,
            {base64:image64})
            .then(
                function(response) {
                    calculateLocal(response);
                },
                function(err) {
                    console.log(err);
                }
            );
    }

    const listSubmit = async event => {
        await event.preventDefault();

        if(localState || urlState) {
            try {
                const request = { username : token.name, name: formData.name }
                const response = await fetch('http://localhost:3000/antri', {
                    method: 'POST',
                    headers: {
                    'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(request)
                });
                const data = await response.json();
                setAntri(data);
                history.push('/list');
            } catch(e) {
                console.log(e);
            }   
        }
    
    }

    const inputsChange = event => {
        setFormData({
            name: event.target.name,
            value: event.target.value
        });

        if(event.target.name == "url") {
            setImageUrl(event.target.value); 
            setUrlBox([]);  
            setShowUrlBoxes([]);
        }

        if(event.target.name == "local") {
            const acceptedImageTypes = ['image/jpeg', 'image/png'];
            
            if(event.target.files[0] != undefined) {
                const reader = new FileReader();
                if(acceptedImageTypes.includes(event.target.files[0].type)) {
                    reader.readAsDataURL(event.target.files[0]);
                    reader.onload = function () {
                        setImageLocal(reader.result); 
                    };
                    reader.onerror = function (error) {
                        console.log('Error: ', error);
                    }; 
                }
                setLocalBox([]); 
                setShowLocalBoxes([]); 
            }     
        }

    };
    const [content, set] = useState(0);



    return(
        <div>
            <div className="container-login100">
                <div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
                        <form onSubmit={listSubmit}>
                            <span class="login100-form-title p-b-32 text-center">
                                Pendaftaran Antrian
                            </span>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Nama lengkap</label>
                                <input type="text" className="form-control" id="exampleInputEmail1"
                                       aria-describedby="emailHelp" placeholder="Enter Name" name="name" onChange={inputsChange} required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Pilih Upload url/local</label><br/>
                                <div className="d-flex justify-content-around mt-1">
                                    <a className="btn btn-primary" onClick={() => set(1)} style={{width: '50%', marginRight: '2px'}}>Url</a>
                                    <a className="btn btn-primary" onClick={() => set(2)} style={{width: '50%', marginLeft: '2px'}}>Local</a>
                                </div>
                            </div>
                            {content == 1? (
                                <div>
                                    <div className="form-group">
                                        <label htmlFor="exampleInputEmail1">URL Foto</label>
                                        <div className="input-group">
                                            <input type="text" className="form-control" id="url" name="url"
                                                    aria-describedby="emailHelp" placeholder="Enter url" onChange={inputsChange}/>
                                            <div class="input-group-append">
                                                <button className="btn btn-secondary" onClick={validateUrl}>Validasi</button>
                                            </div>
                                        </div>
                                        {urlError &&
                                            <p id="urlError" style={{color: 'red'}}>Pastikan foto yang digunakan merupakan foto pribadi anda</p>
                                        }
                                    </div>
                                    <div style={{ position: 'relative' }} className="d-flex justify-content-center">
                                        { imageUrl &&
                                        <img id="urlFace" src={imageUrl} style={{ maxWidth: '100%' }} className="align-self-center" />
                                        }
                                        { showUrlBoxes }
                                        {/* <div className="bounding-box" style={{top: urlBox.topRow, right: urlBox.rightCol, bottom: urlBox.bottomRow, left: urlBox.leftCol }}></div> */}
                                    </div>
                                </div>
                            ): content == 2? (
                                <div>
                                    <div className="form-group">
                                        <label htmlFor="exampleFormControlFile1">Foto Local</label>
                                            <div className="input-group">
                                                <div className="custom-file">
                                                    <input type="file" name="local" className="custom-file-input"  id="exampleFormControlFile1" onChange={inputsChange}/>
                                                    <label class="custom-file-label" for="exampleFormControlFile1">Choose file</label>
                                                </div>
                                                <div class="input-group-append">
                                                    <button className="btn btn-secondary" onClick={validateLocal}>Validasi</button>
                                                </div>
                                            </div>
                                    </div>
                                    {localError &&
                                        <p id="localError" style={{color: 'red'}}>Pastikan foto yang digunakan merupakan foto pribadi anda</p>
                                    }
                                    <div style={{ position: 'relative' }} className="d-flex justify-content-center">
                                        { imageLocal &&
                                        <img id="localFace" src={imageLocal} style={{ maxWidth: '100%' }} className="align-self-center" />
                                        }
                                        { showLocalBoxes }
                                    </div>
                                </div>
                            ) : (
                                <p>Pilih opsi</p>
                            )
                            }
                            <center>
                                <button className="btn btn-primary mt-4" style={{width :50+"%"}}>Mulai Ngantri</button>
                            </center>
                    </form>
                </div>
            </div>
            
        </div>
    )
}
export default AddList;