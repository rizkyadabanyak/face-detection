import React, { useEffect, useState } from 'react'
import Img from './img-landing.png'
import {Link} from "react-router-dom";

 const Landing = ({ token }) =>{
  const [fetchingState, setfetchingState] = useState({message: []});

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('http://localhost:3000/');
        const data = await response.json();
        setfetchingState({message: data.message});
      } catch(e) {
        console.log("Error fetching data");
      }
    }

    fetchData();
  }, [setfetchingState]);

  
  return(
    <div>
        <div className="container-login100">
            <div className="container">
            <div className="row">
                  <div className="col-md-7">
                      <img src={Img} className="img-fluid" alt="Responsive image" width="900"/>
                  </div>
                  <div className="col-md-5 align-self-center">
                      <h2 className="font-weight-bold" style={{fontFamily: "roboto"}}>Yuk Mengantri Dengan Lebih mudah Dan Aman dengan ANT3</h2>
                      <br/>
                      {token
                       ? <Link to='/antri'><button type="button" className="btn btn-warning text-white">Ngantri</button></Link> 
                       : <Link to='/login'><button type="button" className="btn btn-warning text-white">Ngantri</button></Link> 
                      }

                  </div>
              </div>
            </div>
        </div>
    </div>
  )
}
export default Landing;