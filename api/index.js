//import module
const express = require('express');
const bodyParser = require("body-parser");
const bcrypt = require("bcrypt");
const cors = require('cors');
const mongoose = require('mongoose');
require('dotenv').config();

//Konfigurasi module
const app = express();
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
const { Schema } = mongoose;
const saltRounds = 10;

app.use(bodyParser.json());
app.use(cors());

//Schema mongoDB
const usersSchema = new Schema({
  name: String,
  email: String,
  hash: String,
  entries: Number,
  joined: Date
});

const antriSchema = new Schema({
  username: String,
  name: String,
  urutan: Number
});

//Model untuk schema
const User = mongoose.model('User', usersSchema, 'users');
const Antri = mongoose.model('Antri', antriSchema, 'antrian');

app.get('/', (req, res) => {
    res.send({ "message": "Hallo ini dari GET /" });
});

//Masuk ke akun yang dibuat
app.post('/signin', (req, res) => {
  const { email, password } = req.body;
  
  User.findOne({ email: email }, (err, data) => {
    if(err) { return console.log("Error mencari document users dengan email"); }
    if(!data) { return res.json({ message: "Email atau password salah" }); }

    bcrypt.compare(password, data.hash, (err, result) => {
      if(err || !result) { return res.json({ message: "Email atau password salah" }); }
        
      res.json({
          message: "berhasil",
          user: {
            name: data.name,
            token: true
          } 
      });
    })
  });
});

//Membuat akun baru
app.post('/register', (req, res) => {
  const { name, email, password } = req.body;

  User.findOne({ name: name }, (err, nameFound) =>  { 
    if(nameFound) return res.json({ message: "Username sudah terdaftar"});  

    User.findOne({ email: email}, (err, emailFound) => { 
      if(emailFound) return res.json({ message: "Email sudah terdaftar"}); 

      bcrypt.hash(password, saltRounds, (err, hash) => {
        if(err) { return console.log( "Error hashing password" ); }
        
        const registeredUser = new User({
          name: name,
          email: email,
          hash: hash,
          entries: 0,
          joined: new Date()
        });
    
        registeredUser.save(err => {
          if(err) { return console.log("Error menambahkan document ke collection users"); }
          else { res.json({ message: "Registrasi berhasil" }); }
        });
      });
    });
  });
});

//Memberikan nomor antrian
app.post('/antri', (req, res) => {
  const { username, name } = req.body;
  
  Antri.find({}, (err, antrian) => {
    const antrianUser = Antri({
      username: username,
      name: name,
      urutan: antrian.length + 1
    });

    antrianUser.save((err, user) => { res.json(user); });
  });
});

//Memberikan list antrian yang ada
app.get('/antrian', (req, res) => {
  Antri.find({}, (err, users) => { res.json(users); });
});

app.get('/antrian/:username', (req, res) => {
  const { username } = req.params;
  Antri.findOne({username: username}, (err, user) => { 
    res.json(user); 
  });
});

app.listen(process.env.PORT, () => {
  console.log(`Example app listening at http://localhost:${process.env.PORT}`);
})