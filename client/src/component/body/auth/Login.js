import React, { useEffect, useState, useReducer } from 'react'
import {Redirect} from 'react-router';
import { useHistory } from "react-router-dom";
import PropTypes from 'prop-types';

const formReducer = (state, event) => {
  return {
    ...state,
    [event.name]: event.value
  }
 }

export default function Login({ setToken, setAntri }) {
  const [formData, setFormData] = useReducer(formReducer, {});
  const [errorMessage, setErrorMessage] = useState();
  let history = useHistory();

  const handleSubmit = async event => {
    await event.preventDefault();
    try {
      const response = await fetch('http://localhost:3000/signin', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      });
      const data = await response.json();

      if (data.message == 'berhasil'){
        localStorage.setItem('token', JSON.stringify(data.user));
        const response = await fetch(`http://localhost:3000/antrian/${data.user.name}`);
        const status = await response.json();
        
        await setAntri(status);
        await setToken(data.user);
        history.push('/');
      } else {
        setErrorMessage(data.message);
      }

      console.log(data.message);
    } catch(e) {
      console.log(e);
    }
  }

  const inputsChange = event => {
    setFormData({
      name: event.target.name,
      value: event.target.value
    });
  };

  const passVisibility = () => {
    const inputPassword = document.getElementById("inputPassword");
    const eye = document.getElementById("eye");

    if(inputPassword.type == "password") {
      inputPassword.setAttribute('type', 'text');
      eye.setAttribute('class', 'fa fa-eye-slash');
    } else {
      inputPassword.setAttribute('type', 'password');
      eye.setAttribute('class', 'fa fa-eye');
    }
  }

    return (
        <div>
       <div className="container-login100">
          
          <div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
            <form class="login100-form d-flex flex-column validate-form flex-sb flex-w" onSubmit={handleSubmit}>
              <span class="login100-form-title p-b-32 text-center">
                Login
              </span>

              {errorMessage &&
                <h6 className="mb-3 text-center" style={{color: 'red'}}>
                  { errorMessage  }
                </h6>
              }

              <div class="txt1 p-b-11">
                Email
              </div>
              <div class="wrap-input100 validate-input m-b-36" data-validate = "email is required">
                <input class="input100" type="email" name="email" onChange={inputsChange} required/>
                <span class="focus-input100"></span>
              </div>
              
              <span class="txt1 p-b-11">
                Password
              </span>
              <div class="wrap-input100 validate-input m-b-12" data-validate = "Password is required">
                <span class="btn-show-pass">
                  <i id="eye" class="fa fa-eye" onClick={passVisibility}></i>
                </span>
                <input class="input100" type="password" id="inputPassword" name="password" onChange={inputsChange} required/>
                <span class="focus-input100"></span>
              </div>
              
              <div class="flex-sb-m w-full p-b-48">
                <div class="contact100-form-checkbox">
                  <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me"/>
                  <label class="label-checkbox100" for="ckb1">
                    Remember me
                  </label>
                </div>

                <div>
                  <a href="#" class="txt3">
                    Forgot Password?
                  </a>
                </div>
              </div>

              <div class="container-login100-form-btn">
                <button class="login100-form-btn" style={{width :100+"%"}}>
                  Login
                </button>
              </div>

            </form>
          </div>
        </div>
      </div>
    )
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired
}
