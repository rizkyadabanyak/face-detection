import React,{component, Fragment, useEffect} from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import { BrowserRouter, Route, Router,Link } from 'react-router-dom'

export default function Navbar({ token, clearToken }) {
    return (
        <div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">Ant3</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <Link to='/'><a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a></Link>
                    </li>
                        <li className="nav-item active">
                            <Link to='/list'><a className="nav-link" href="#">Daftar Antrian <span
                                className="sr-only">(current)</span></a></Link>
                        </li>

                    </ul>

                    {/* ? = kalo udh login (if token), : = kalo belom (else) */}
                    { token
                        ? <Link to='/'><button type="button" class="btn btn-primary" onClick={clearToken} style={{marginRight : 10 +"px"}}>Logout</button></Link> 
                        : <Link to='/login'><button type="button" class="btn btn-primary" style={{marginRight : 10 +"px"}}>Login</button></Link>
                    }
                    { token
                        ? <Link to='/antri'><button type="button" class="btn btn-warning text-white">Antri</button></Link>
                        : <Link to='/regis'><button type="button" class="btn btn-warning text-white">Register</button></Link>
                    }
                </div>
            </nav>

            
        </div>
        

    )
}
