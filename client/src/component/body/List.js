import React, { useEffect, useState } from 'react'
import { BrowserRouter, Route, Router,Link } from 'react-router-dom'

const List = ({ antri, token }) =>{
    const [showAntrian, setShowAntrian] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
              const response = await fetch('http://localhost:3000/antrian');
              const data = await response.json();
              await setShowAntrian(data);
            } catch(e) {
              console.log("Error fetching data");
            }
          }
      
          fetchData();
    }, [setShowAntrian]);

    const scrollToAntrian = () => {
        const urutan = document.getElementById(antri.urutan)
        urutan.focus();

        urutan.scrollIntoView({
            behavior: 'auto',
            block: 'center',
            inline: 'center'
        });
    }
    return(
        <div style={{backgroundColor : "#ebebeb",height: 1500+"px"}} >
            <div className="container text-center" >
                <br/><br/>
                {antri 
                    ? 
                    <div>
                        <h2>Anda mendapatkan urutan nomor {antri.urutan}</h2>
                        <button onClick={scrollToAntrian} className="btn btn-warning text-white" href={"#" + antri.urutan}>Tunjukkan antrian saya</button>
                    </div>
                    : token 
                        ? <Link to='/antri'><button type="button" className="btn btn-warning text-white">Mulai Ngantri</button></Link>
                        : <Link to='/login'><button type="button" className="btn btn-warning text-white">Mulai Ngantri</button></Link>
                    
                }
                
                <br/><br/>
                <div className="row">
                    { 
                        showAntrian.map(antrian => {
                            return (
                            <div key={antrian._id} className="col-sm-4 mb-4">
                                <div id={antrian.urutan} tabindex="0" className="card">
                                    <div className="card-body">
                                        <h5 className="card-title">{antrian.urutan}</h5>
                                        <p className="card-text">{antrian.name}</p>
                                    </div>
                                </div>
                            </div>
                            );
                        }) 
                    }
                </div>
            </div>
        </div>
    )
}
export default List;