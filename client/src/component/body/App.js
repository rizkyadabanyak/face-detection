import { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import { BrowserRouter, Route, Router,Link, Switch, Redirect } from 'react-router-dom'
import Register from './auth/Register'
import Login from './auth/Login'
import Landing from '../body/Landing'


import React,{component, Fragment} from 'react'
import Navbar from '../nav/Navbar';
import AddList from './AddList';
import List from "./List";

export default function App() {
  const [antri, setAntri] = useState();

  const getToken = () => {
    const tokenString = localStorage.getItem('token');
    const userToken = JSON.parse(tokenString);
    return userToken;
  }

  useEffect(async () => {
    const getUser = async () => {
      if(token) {
        const response = await fetch(`http://localhost:3000/antrian/${token.name}`);
        const status = await response.json();

        await setAntri(status);
      } else {
        setAntri();
        // return false;
      }
    }

    getUser();
  }, [setAntri]);


  const [token, setToken] = useState(getToken());
  

  const clearToken = () => { 
      localStorage.clear('token');
      setToken();
      setAntri();
  }

  return( 
    <BrowserRouter>
      <Navbar token={token} clearToken={clearToken}/> 
      <Switch>
        <Route exact path='/'>
          <Landing token={token} />
        </Route>
        <Route path='/login' >
          <Login setToken={setToken} setAntri={setAntri}/>
        </Route>
        <Route path='/regis'>
          <Register />
        </Route>
        <Route path='/antri'>
          {antri 
            ? <List token={token} antri={antri}/>
            : <AddList token={token} setAntri={setAntri}/>
          }
          
        </Route>
        <Route path='/list'>
          <List token={token} antri={antri}/>
        </Route>
      </Switch>
  </BrowserRouter>
    );

}

